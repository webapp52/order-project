import { IsNotEmpty, IsPositive } from 'class-validator';
class CreatedOrderItemDto {
  @IsNotEmpty()
  productId: number;
  @IsNotEmpty()
  @IsPositive()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
