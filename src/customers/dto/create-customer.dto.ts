import { IsNotEmpty, IsPositive } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  @IsPositive()
  age: number;
  @IsNotEmpty()
  tel: string;
  @IsNotEmpty()
  gender: string;
}
